const mysql = require('./db');
const express = require('express');

const app = express();
const port = process.env.PORT || 3001
app.use(express.json());

app.use("",require("./routes/userWithoutAuth"));


app.listen(port,()=>{
    console.log(`Backend is live at http://localhost:${port}`);
})