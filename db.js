const mysql = require('mysql')
require('dotenv').config();


var con= mysql.createConnection({
        host:"localhost",
        database:process.env.DB,
        user:process.env.DB_USER,
        password:process.env.DB_PASS
    });

con.connect((e)=>{
    if(e) console.log("error for db:",e);
    console.log("Successfully Connected to DB!");
})

module.exports = con;